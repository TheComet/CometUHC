package com.bodilgames.bodilminigameuhc.task;

import com.bodilgames.bodilminigameuhc.BodilMinigameUHC;
import org.bukkit.scheduler.BukkitRunnable;

public class GameTimerTask extends BukkitRunnable {
    private final BodilMinigameUHC bp;
    private Integer hours = Integer.valueOf(0);
    private Integer minutes = Integer.valueOf(0);
    private Integer seconds = Integer.valueOf(0);

    public GameTimerTask(BodilMinigameUHC bp) {
        this.bp = bp;
        //hours = bp.getGame().getHoursLeft();
        //minutes = bp.getGame().getMinutesLeft();
        //seconds = bp.getGame().getSecondsLeft();
    }

    @Override
    public void run() {
        seconds -= 1;
        if (seconds <= 0) {
            seconds += 60;
            minutes -= 1;
        }
        if (minutes <= 0) {
            minutes += 60;
            hours -= 1;
        }
        if (hours < 0) {
            //Run end game script here
        }
        bp.getGame().setHoursLeft(hours);
        bp.getGame().setMinutesLeft(minutes);
        bp.getGame().setSecondsLeft(seconds);
    }
}
