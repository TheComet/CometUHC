package com.bodilgames.bodilminigameuhc;

import com.bodilgames.bodilminigameuhc.task.GameTaskManager;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Random;

public class GameUtil {
    private BodilMinigameUHC plugin = null;
    private World gameWorld = null;
    private World lobby = null;
    //private int worldSize;
    private List<Player> pList;

    private GameTaskManager gT;

    private Integer hoursLeft = 2;
    private Integer minutesLeft = 0;
    private Integer secondsLeft = 0;

    public GameUtil(BodilMinigameUHC plugin) {
        this.plugin = plugin;
        this.gT = new GameTaskManager(this);
        gameWorld = (this.plugin.getServer().getWorld("UHCWorld"));
        lobby = (this.plugin.getServer().getWorld("Lobby"));
    }

    public void gameStart(int worldSize) {
        //First thing that needs to be done is start the countdown
        //after the start time is up, each player needs to be teleported to the edges of the map
        //as well, the world boarder needs to be set.

        //intlWorld(worldSize);
        //startTimer();
        intlPlayers(worldSize);
    }

    public void startTimer() {
        //This function will start the starting countdown
        //for now, it will be displayed as title messages
        //later, it will go on the scoreboard when it is working
        //make sure that the start time is set to true, so it does not restart on new player joins
        //Make sure to check and see if the game start has already begun
        hoursLeft = 2;
        minutesLeft = 0;
        secondsLeft = 0;

        gT.getTimer().runTaskTimer(plugin, 20, 20);
        gT.getGcT().runTaskTimer(plugin, 1200, 600);

        //plugin.getScoreboard().getScoreboard();

    }

    public void intlWorld(int size) {
        if (gameWorld != null) {
            gameWorld.getWorldBorder().setCenter(gameWorld.getSpawnLocation());
            gameWorld.getWorldBorder().setSize(size);
            plugin.getWorldmanager().loadChunkRadius(size / 2, gameWorld.getSpawnLocation());
            gameWorld.setGameRuleValue("doDaylightCycle", "false");
            gameWorld.setTime(6000L);
            gameWorld.setStorm(false);
            gameWorld.setGameRuleValue("naturalRegeneration", "false");
            gameStart(size);
        }
    }

    public void intlPlayers(int edge) {
        //Should be changed out to use a list, refrence to the UUID, rather than a player array.
        Random rand = new Random();
        //edge is defined by the boarder, but we need to get half of that t have the correct radius
        edge = edge / 2;
        pList = lobby.getPlayers();
        Location worldSpawn = gameWorld.getSpawnLocation();
        Location startPoint = worldSpawn;
        ItemStack startItems = new ItemStack(Material.COMPASS);
        double degrees;
        double rad;
        double xLoc;
        double zLoc;
        double yLoc;
        for (int i = 0; i < pList.size(); i++) {
            //each player needs to be teleported to the edge of the world
            if (pList.get(i) != null) {
                Player p = pList.get(i);
                Inventory pInv = p.getInventory();
                degrees = rand.nextInt(360);
                rad = Math.toRadians(degrees);
                xLoc = (Math.cos(rad) * edge - 5) + worldSpawn.getX();
                zLoc = (Math.sin(rad) * edge - 5) + worldSpawn.getZ();
                yLoc = worldSpawn.getWorld().getHighestBlockAt(((int) xLoc), ((int) zLoc)).getY() + 1;
                startPoint.setX(xLoc);
                startPoint.setY(yLoc);
                startPoint.setZ(zLoc);
                startPoint.setWorld(gameWorld);

                pInv.clear();
                pInv.setItem(pInv.firstEmpty(), startItems);
                p.setFoodLevel(20);
                p.setSaturation(14.0F);
                p.setHealth(20.0D);
                p.setCompassTarget(worldSpawn);
                p.setGameMode(GameMode.SURVIVAL);

                p.teleport(startPoint);
            }

        }

    }

    public Integer getHoursLeft() {
        return hoursLeft;
    }

    public void setHoursLeft(Integer hoursLeft) {
        this.hoursLeft = hoursLeft;
    }

    public Integer getMinutesLeft() {
        return minutesLeft;
    }

    public void setMinutesLeft(Integer minutesLeft) {
        this.minutesLeft = minutesLeft;
    }

    public Integer getSecondsLeft() {
        return secondsLeft;
    }

    public void setSecondsLeft(Integer secondsLeft) {
        this.secondsLeft = secondsLeft;
    }

    public BodilMinigameUHC getPlugin() {
        return plugin;
    }
}
