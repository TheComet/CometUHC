package com.bodilgames.bodilminigameuhc;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class UHCListiners implements Listener {
    private BodilMinigameUHC plugin = null;


    public UHCListiners(BodilMinigameUHC plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onLogin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        p.teleport(plugin.getServer().getWorld("Lobby").getSpawnLocation());
        p.setGameMode(GameMode.CREATIVE);
        //List<Player> pList = (List<Player>) p.getServer().getOnlinePlayers();

        plugin.getLogger().info(p.getName() + " has joined the UHC lobby!");
        //if (pList.size() >= plugin.getServer().getMaxPlayers()) {plugin.getGame().intlWorld(50000);}
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player p = event.getEntity();
        p.setGameMode(GameMode.SPECTATOR);
        p.setHealth(20);
    }


}
