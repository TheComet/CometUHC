package com.bodilgames.bodilminigameuhc;

import com.bodilgames.bodilcore.utils.reflection.HandleReflection;
import org.bukkit.*;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import static com.bodilgames.bodilcore.utils.reflection.HandleReflection.getHandle;

public class UHCWorldUtilities {
    private static BodilMinigameUHC b = null;

    public UHCWorldUtilities(BodilMinigameUHC b) {
        this.b = b;
    }

    public boolean deleteWorld(File path) {
        if (path.isDirectory()) {
            File[] files;
            int i2 = (files = path.listFiles()).length;
            for (int i = 0; i < i2; i++) {
                File path2 = files[i];
                if (!deleteWorld(path2)) {
                    return false;
                }
            }
        }
        return (path.delete());
    }

    public void unloadWorld(String worldname) {
        World uWorld = Bukkit.getWorld(worldname);
        if (!uWorld.equals(null)) {
            Bukkit.getServer().unloadWorld(uWorld, true);
        }
    }

    public static void createRandWorld(String worldName) {
        List<World> wList = b.getServer().getWorlds();
        int worlds = wList.size();
        boolean var2 = false;
        for (int i = 0; i < worlds; i++) {
            if (wList.get(i).getName().equals(worldName)) {
                var2 = true;
            }
        }

        if (!var2) {
            WorldCreator worldGenerator = WorldCreator.name(worldName);

            worldGenerator.createWorld();
        }
    }

    public static void regenChunkRadius(int radius, Location center) {
        Chunk[][] chunkArr;
        chunkArr = selectChunkRadius(radius, center);
        for (Chunk[] arr2 : chunkArr) {
            for (Chunk genChunk : arr2) {
                genChunk.unload();
                genChunk.getWorld().regenerateChunk(genChunk.getX(), genChunk.getZ());
                genChunk.load();

            }
        }
    }

    public static void loadChunkRadius(int radius, Location center) {
        center.getChunk().load();
        World loadWorld = center.getWorld();

        for (int i = -radius; i <= radius; i++) {
            for (int i2 = -radius; i2 <= radius; i2++) {
                loadWorld.getChunkAt(i + (int) center.getX(), i2 + (int) center.getZ()).load();
            }
        }

        /*Chunk[][] chunkArr;
        chunkArr = selectChunkRadius(radius, center, type);
        for (Chunk[] arr2 : chunkArr)
        {
            for(Chunk genChunk : arr2)
            {
                genChunk.load();
            }
        }*/
    }

    public static Chunk[][] selectChunkRadius(int radius, Location center) {
        Chunk startChunk = center.getChunk();
        World chunkWorld = center.getWorld();
        Chunk[][] chunkArr = null;

        for (int xi = startChunk.getX() - radius; xi < startChunk.getX() + radius; xi++) {
            for (int zi = startChunk.getZ() - radius; zi < startChunk.getZ() + radius; zi++) {
                chunkArr[xi + radius][zi + radius] = chunkWorld.getChunkAt(xi, zi);

                /*if (type.equals("circle") && (xi^2 + zi^2) >= radius)
                {
                    chunkArr[xi][zi] = null;
                }*/
            }
        }

        return chunkArr;
    }

    public static Object getWorldData(World world) {
        Object handle = HandleReflection.getHandle(world);
        Object nmsEntity = null;
        Method getWorldData = getMethod(handle.getClass(), "getWorldData");

        try {
            nmsEntity = getWorldData.invoke(handle);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return nmsEntity;
    }

    public static void setWorldSeed(long seed, World world) {
        Object worldData = getWorldData(world);
        Field b = null;

        try {
            b = worldData.getClass().getField("b");

            if (b != null) {
                b.setAccessible(true);
                b.setLong(world, seed);
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static Field getField(String fieldName, Class<?> clazz, boolean ignoreCase) throws SecurityException, NoSuchFieldException {
        if (ignoreCase) {
            Field[] fieldArray = clazz.getDeclaredFields();
            int arrayLength = fieldArray.length;

            for (int i = 0; i < arrayLength; ++i) {
                Field f = fieldArray[i];
                if (f.getName().equalsIgnoreCase(fieldName)) {
                    return f;
                }
            }

            throw new NoSuchFieldException(fieldName);
        } else {
            return clazz.getDeclaredField(fieldName);
        }
    }

    public static Method getMethod(Class<?> clazz, String method) {
        for (Method m : clazz.getDeclaredMethods()) {
            if (m.getName().equals(method)) {
                return m;
            }
        }

        return null;
    }
}
