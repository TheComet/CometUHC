package com.bodilgames.bodilminigameuhc.task;

import com.bodilgames.bodilminigameuhc.BodilMinigameUHC;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class GameCompassTask extends BukkitRunnable {
    private final BodilMinigameUHC bp;

    public GameCompassTask(BodilMinigameUHC bp) {
        this.bp = bp;
    }

    @Override
    public void run() {
        Player curPlayer;
        Player tarPlayer;
        Player compTarg = null;

        double dist = 0;
        double targDistance;
        List<Player> pList;

        pList = this.bp.getServer().getWorlds().get(1).getPlayers();
        for (int i = 0; i <= pList.size(); i++) {
            curPlayer = pList.get(i);
            dist = 500000000000.0D;
            for (int i2 = 0; i2 <= pList.size(); i2++) {
                if (i2 != i && pList.get(i2).getGameMode() == GameMode.SURVIVAL) {
                    tarPlayer = pList.get(i2);
                    targDistance = calcDist(curPlayer, tarPlayer);

                    if (targDistance < dist) {
                        dist = targDistance;
                        compTarg = tarPlayer;
                    }
                }
            }

            curPlayer.setCompassTarget(compTarg.getLocation());
        }
    }

    public double calcDist(Player p1, Player p2) {
        double xDist = p1.getLocation().getX() - p2.getLocation().getX();
        double zDist = p1.getLocation().getZ() - p2.getLocation().getZ();
        double dist = Math.sqrt((xDist * xDist) + (zDist * zDist));

        return dist;
    }
}
