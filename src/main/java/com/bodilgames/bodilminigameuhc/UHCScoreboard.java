package com.bodilgames.bodilminigameuhc;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.*;

public class UHCScoreboard {
    private BodilMinigameUHC plugin = null;

    ScoreboardManager sbM = Bukkit.getScoreboardManager();
    Scoreboard sb = sbM.getNewScoreboard();

    public UHCScoreboard(BodilMinigameUHC plugin) {
        this.plugin = plugin;
    }

    public void intlScoreboard() {
        Objective timer = this.sb.registerNewObjective("timer", "dummy");
        timer.setDisplaySlot(DisplaySlot.SIDEBAR);
        timer.setDisplayName("Timer");
    }

    public Scoreboard getScoreboard() {
        return this.sb;
    }

    public ScoreboardManager getScoreboardManager() {
        return this.sbM;
    }
}
