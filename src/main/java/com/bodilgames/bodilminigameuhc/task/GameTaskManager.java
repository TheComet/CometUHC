package com.bodilgames.bodilminigameuhc.task;

import com.bodilgames.bodilminigameuhc.GameUtil;

public class GameTaskManager {
    private GameUtil b;
    private GameCompassTask gcT;
    private GameTimerTask timer;

    public GameTaskManager(GameUtil b) {
        this.b = b;
        gcT = new GameCompassTask(b.getPlugin());
        timer = new GameTimerTask(b.getPlugin());

    }

    public GameTimerTask getTimer() {
        return timer;
    }

    public void setTimer(GameTimerTask timer) {
        this.timer = timer;
    }

    public GameCompassTask getGcT() {
        return gcT;
    }

    public void setGcT(GameCompassTask gcT) {
        this.gcT = gcT;
    }
}
