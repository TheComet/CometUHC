package com.bodilgames.bodilminigameuhc;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class BodilMinigameUHC extends JavaPlugin {
    private UHCCommands commandManager = null;
    private UHCWorldUtilities worldmanager = null;
    private UHCListiners listiner = null;
    private UHCScoreboard scoreboardManager = null;
    private GameUtil game = null;
    private int worldCount;


    @Override
    public void onEnable() {
        // TODO Insert logic to be performed when the plugin is enabled
        getLogger().info(getDescription().getName() + " onEnable has been invoked!");
        this.commandManager = new UHCCommands(this);
        this.worldmanager = new UHCWorldUtilities(this);
        this.listiner = new UHCListiners(this);
        this.game = new GameUtil(this);
        this.scoreboardManager = new UHCScoreboard(this);

        worldmanager.createRandWorld("UHCWorld");
        worldmanager.loadChunkRadius(50, this.getServer().getWorld("UHCWorld").getSpawnLocation());

        if (this.getServer().getWorld("Lobby") == null) {
            worldmanager.createRandWorld("Lobby");
        }

        getServer().getPluginManager().registerEvents(listiner, this);
    }

    @Override
    public void onDisable() {
        // TODO Insert logic to be performed when the plugin is disabled
        getLogger().info(getDescription().getName() + " onDisable has been invoked!");
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!command.getName().equalsIgnoreCase("start")) {
            return false;
        }
        if (label.equalsIgnoreCase("start")) {
            this.game.intlWorld(500);
        }
        return true;
    }

    public GameUtil getGame() {
        return this.game;
    }

    public UHCScoreboard getScoreboard() {
        return this.scoreboardManager;
    }

    public UHCWorldUtilities getWorldmanager() {
        return worldmanager;
    }
}
